@core @core_local @uji @exammode
Feature: userblock_exammode
    Como estudiante de un curso en modo examen no debo poder acceder al bloque de ficheros privados del Tauler

    Background:
      Given the following "users" exist:
        | username | firstname | lastname | email |
        | student1 | Student | 1 | student1@example.com |
        | teacher1 | Teacher | 2 | teacher1@example.com |
      And the following "courses" exist:
        | fullname | shortname | format |
        | Course 1 | C1 | topics |
      And the following "course enrolments" exist:
        | user | course | role |
        | student1 | C1 | student |
        | teacher1 | C1 | editingteacher |
      And the following "blocks" exist:
        | blockname     | contextlevel | reference | pagetypepattern | defaultregion |
        | private_files | System       | 1         | my-index        | side-post     |
      And the following config values are set as admin:
        | customscripts | #dirroot#/local/exammode/customscripts |

    @javascript
    Scenario: Como estudiante en modo examen, no puedo acceder al perfil de usuario
      Given I log in as "student 1"
      And I follow "Profile" in the user menu
      And I should see "User details"
      And I log out
      And I log in as "teacher1"
      And I am on "Course 1" course homepage
      And I click on "More" "link" in the ".secondary-navigation" "css_element"
      And I click on "Exam Mode" "link"
      And I click on "New exam" "button"
      And I click on "Save changes" "button"
      And I log out
      And I run the scheduled task "\local_exammode\task\update_exammode_users"
      And I log in as "student1"
      When I follow "Profile" in the user menu
      Then I should not see "User details"

    Scenario: Como estudiante de un curso en modo examen no debo poder acceder al bloque de ficheros privados del Tauler
        Given I log in as "student1"
        And "Private files" "block" should exist
        And I log out 
        When I log in as "teacher1"
        And I am on "Course 1" course homepage
        And I click on "More" "link" in the ".secondary-navigation" "css_element"
        And I click on "Exam Mode" "link"
        And I click on "New exam" "button"
        And I click on "Save changes" "button"
        And I log out
        And I run the scheduled task "\local_exammode\task\update_exammode_users"
        Then I log in as "student1"
        And "Private files" "block" should not exist

