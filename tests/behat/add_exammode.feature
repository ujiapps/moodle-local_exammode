@core @core_local @uji @exammode
Feature: add_exammode
    Como profesor de un curso puedo añadir un modo examen

    Background:
      Given the following "users" exist:
        | username | firstname | lastname | email |
        | student1 | Student | 1 | student1@example.com |
        | teacher1 | Teacher | 2 | teacher1@example.com |
      And the following "courses" exist:
        | fullname | shortname | format |
        | Course 1 | C1 | topics |
      And the following "course enrolments" exist:
        | user | course | role |
        | student1 | C1 | student |
        | teacher1 | C1 | editingteacher |

    Scenario: Como profesor de un curso puedo añadir un modo examen
        Given I log in as "teacher1"
        When I am on "Course 1" course homepage
        And I click on "More" "link" in the ".secondary-navigation" "css_element"
        And I click on "Exam Mode" "link"
        And I click on "New exam" "button"
        And I click on "Save changes" "button"
        Then I should see "Exam mode scheduled on" in the ".alert-success" "css_element"

